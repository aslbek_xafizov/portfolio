from os import name
from django.urls import path
from .views import index, about, services, scill, contact


urlpatterns = [
    path('', index, name='index'),
    path('about/', about, name='about'),
    path('services/', services, name='services'),
    path('scill/', scill, name='scill'),
    path('contact/', contact, name='contact')
]