from django.shortcuts import redirect, render, get_object_or_404
from .models import Blog
from .forms import BlogForm
# Create your views here.

def blog(request):
    abs = Blog.objects.all()
    blog_all = Blog.objects.all().count()
    return render(request, 'blog.html', {'abs':abs,'blog_all':blog_all})

def deteil(request,id):
    dog = get_object_or_404(Blog, id=id)
    dog.blog_count = dog.blog_count + 1  #ko'ridshlar soni
    dog.save()    #saqlsh
    return render(request, 'deteil.html', {'blog':dog})

def createBlog(request):
    form = BlogForm()
    if request.method == 'POST':
        form = BlogForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('blog')
    # else:
    #     form = BlogForm()

    return render(request, 'createBlog.html', {'form': form})

def ubdedBlog(request,id):
    blog =get_object_or_404(Blog,id=id)
    if request.method == 'POST':
        form = BlogForm(request.POST, instance=blog)
        if form.is_valid():
            form.save()
            return redirect('blog')
    else:
        form = BlogForm(instance=blog)
    return render(request, 'ubdedBlog.html', {'form': form, 'blog':blog})

def deleteBlog(request,id):
    blog = get_object_or_404(Blog,id=id)
    form = BlogForm(instance=blog)
    if request.method == 'POST':
        blog.delete()
        return redirect('blog')

    return render(request, 'deleteBlog.html', {'form': form, 'blog':blog})
