from django.urls import path
from .views import blog, deteil, createBlog, ubdedBlog, deleteBlog



urlpatterns = [
    path('', blog, name='blog'),
    path('deteil/<int:id>', deteil, name='deteil'),
    path('createBlog/', createBlog, name='createBlog'),
    path('ubdedBlog/<int:id>', ubdedBlog, name='ubdedBlog'),
    path('deleteBlog/<int:id>', deleteBlog, name='deleteBlog')
]