from django.db.models import fields
from django.forms import ModelForm
from .models import Blog

class BlogForm(ModelForm):
    class Meta:
        model = Blog
        # fields = '__all__'
        fields = ['title', 'body', 'img', 'author']